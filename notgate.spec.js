var NotGate = require("./notgate");
var assert = require("assert");

describe("NotGate", () => {
	let notValue;
	beforeEach(() => {
		notValue = new NotGate();
	});

	it("should not throw error when created", () => {
		assert.doesNotThrow(() => new NotGate());
	});
	it("should output 1 when input value is 0", () => {
		notValue.in.off();
		assert.equal(notValue.value, 1);
	});
	it("should output 0 when input value is 1", () => {
		notValue.in.on();
		assert.equal(notValue.value, 0);
	});
});

var Switch = require("./switch");
var FullAdder = require("./full_adder");

function BitAdder4() {
	this.s0 = 0;
	this.s1 = 0;
	this.s2 = 0;
	this.s3 = 0;
	this.cout = 0;
	this.in0 = new Switch();
	this.in1 = new Switch();
	this.in2 = new Switch();
	this.in3 = new Switch();
	this.in4 = new Switch();
	this.in5 = new Switch();
	this.in6 = new Switch();
	this.in7 = new Switch();
	this.cin = new Switch();
}

BitAdder4.prototype.calculate = function() {
	let a0 = new FullAdder();
	if(this.in0.value === 1) {
		a0.in0.on();
	} else {
		a0.in0.off();
	}
	if(this.in1.value === 1) {
		a0.in1.on();
	} else {
		a0.in1.off();
	}
	if(this.cin.value === 1) {
		a0.cin.on();
	} else {
		a0.cin.off();
	}
	let a1 = new FullAdder();
	if(this.in2.value === 1) {
		a1.in0.on();
	} else {
		a1.in0.off();
	}
	if(this.in3.value === 1) {
		a1.in1.on();
	} else {
		a1.in1.off();
	}
	if(a0.carry === 1) {
		a1.cin.on();
	} else {
		a1.cin.off();
	}
	let a2 = new FullAdder();
	if(this.in4.value === 1) {
		a2.in0.on();
	} else {
		a2.in0.off();
	}
	if(this.in5.value === 1) {
		a2.in1.on();
	} else {
		a2.in1.off();
	}
	if(a1.carry === 1) {
		a2.cin.on();
	} else {
		a2.cin.off();
	}
	let a3 = new FullAdder();
	if(this.in6.value === 1) {
		a3.in0.on();
	} else {
		a3.in0.off();
	}
	if(this.in7.value === 1) {
		a3.in1.on();
	} else {
		a3.in1.off();
	}
	if(a2.carry === 1) {
		a3.cin.on();
	} else {
		a3.cin.off();
	}
	this.s0 = a0.sum;
	this.s1 = a1.sum;
	this.s2 = a2.sum;
	this.s3 = a3.sum;
	this.cout = a3.carry;
}

BitAdder4.prototype.__defineGetter__("sum0", function() {
	this.calculate();
	return this.s0;
});

BitAdder4.prototype.__defineGetter__("sum1", function() {
	this.calculate();
	return this.s1;
});

BitAdder4.prototype.__defineGetter__("sum2", function() {
	this.calculate();
	return this.s2;
});

BitAdder4.prototype.__defineGetter__("sum3", function() {
	this.calculate();
	return this.s3;
});

BitAdder4.prototype.__defineGetter__("carry", function() {
	this.calculate();
	return this.cout;
});

module.exports = BitAdder4;

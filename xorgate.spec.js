var XorGate = require("./xorgate");
var assert = require("assert");

describe("XorGate", () => {
	let orValue;
	beforeEach(() => {
		xorValue = new XorGate();
	});

	it("should not throw error when created", () => {
		assert.doesNotThrow(() => new XorGate());
	});
	it("should output 0 when both values are 1", () => {
		xorValue.in0.on();
		xorValue.in1.on();
		assert.equal(xorValue.value, 0);
	});
	it("should output 1 when either values are 1", () => {
		xorValue.in0.on();
		xorValue.in1.off();
		assert.equal(xorValue.value, 1);

		xorValue.in0.off();
		xorValue.in1.on();
		assert.equal(xorValue.value, 1);
	});
	it("should output 0 when both values are 0", () => {
		xorValue.in0.off();
		xorValue.in1.off();
		assert.equal(xorValue.value, 0);
	});
});

var Switch = require("./switch");

function AndGate() {
	this.val = 0;
	this.in0 = new Switch();
	this.in1 = new Switch();
}

AndGate.prototype.__defineGetter__("value", function() {return this.in0.value && this.in1.value});

module.exports = AndGate;

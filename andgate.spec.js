var AndGate = require("./andgate");
var assert = require("assert");

describe("AndGate", () => {
	let andValue;
	beforeEach(() => {
		andValue = new AndGate();
	});

	it("should not throw error when created", () => {
		assert.doesNotThrow(() => new AndGate());
	});
	it("should output 1 when both values are 1", () => {
		andValue.in0.on();
		andValue.in1.on();
		assert.equal(andValue.value, 1);
	});
	it("should output 0 when either values are 0", () => {
		andValue.in0.on();
		andValue.in1.off();
		assert.equal(andValue.value, 0);

		andValue.in0.off();
		andValue.in1.on();
		assert.equal(andValue.value, 0);

		andValue.in0.off();
		andValue.in1.off();
		assert.equal(andValue.value, 0);
	});
});

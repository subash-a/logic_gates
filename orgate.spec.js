var OrGate = require("./orgate");
var assert = require("assert");

describe("OrGate", () => {
	let orValue;
	beforeEach(() => {
		orValue = new OrGate();
	});

	it("should not throw error when created", () => {
		assert.doesNotThrow(() => new OrGate());
	});
	it("should output 1 when both values are 1", () => {
		orValue.in0.on();
		orValue.in1.on();
		assert.equal(orValue.value, 1);
	});
	it("should output 1 when either values are 1", () => {
		orValue.in0.on();
		orValue.in1.off();
		assert.equal(orValue.value, 1);

		orValue.in0.off();
		orValue.in1.on();
		assert.equal(orValue.value, 1);
	});
	it("should output 0 when both values are 0", () => {
		orValue.in0.off();
		orValue.in1.off();
		assert.equal(orValue.value, 0);
	});
});

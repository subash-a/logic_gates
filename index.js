var Switch = require("./switch");
var AndGate = require("./andgate");
var OrGate = require("./orgate");
var NotGate = require("./notgate");
var XorGate = require("./xorgate");
var HalfAdder = require("./half_adder");
var FullAdder = require("./full_adder");
var BitAdder4 = require("./4bitadder");

function testGates() {
	console.log("All Logic Gates are ready!");
}

testGates();

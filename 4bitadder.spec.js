var BitAdder4 = require("./4bitadder");
var assert = require("assert");

describe("BitAdder4", () => {
	let ba4Value;
	beforeEach(() => {
		ba4Value = new BitAdder4();
	});

	it("should not throw error when created", () => {
		assert.doesNotThrow(() => new BitAdder4());
	});
	describe("with input carry as 0", () => {
		it("should output 1111 as sum when inputs are 1001 and 0110", () => {
			ba4Value.in0.on();
			ba4Value.in1.off();
			ba4Value.in2.off();
			ba4Value.in3.on();
			ba4Value.in4.off();
			ba4Value.in5.on();
			ba4Value.in6.on();
			ba4Value.in7.off();
			assert.equal(ba4Value.sum0, 1);
			assert.equal(ba4Value.sum1, 1);
			assert.equal(ba4Value.sum2, 1);
			assert.equal(ba4Value.sum3, 1);
			assert.equal(ba4Value.carry, 0);
		});
	});

	describe("with input carry as 1", () => {
		it("should output 1111 as sum when inputs are 1001 and 0110", () => {
			ba4Value.in0.on();
			ba4Value.in1.off();
			ba4Value.in2.off();
			ba4Value.in3.on();
			ba4Value.in4.off();
			ba4Value.in5.on();
			ba4Value.in6.on();
			ba4Value.in7.off();
			ba4Value.cin.on();
			assert.equal(ba4Value.sum0, 0);
			assert.equal(ba4Value.sum1, 0);
			assert.equal(ba4Value.sum2, 0);
			assert.equal(ba4Value.sum3, 0);
			assert.equal(ba4Value.carry, 1);
		});
	});
});

var Switch = require("./switch");

function XorGate() {
	this.val = 0;
	this.in0 = new Switch();
	this.in1 = new Switch();
}

XorGate.prototype.__defineGetter__("value", function() {return this.in0.value !== this.in1.value ? 1 : 0});

module.exports = XorGate;

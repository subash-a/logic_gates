var Switch = require("./switch");
var XorGate = require("./xorgate");
var AndGate = require("./andgate");

function FullAdder() {
	this.cin = new Switch();
	this.in0 = new Switch();
	this.in1 = new Switch();
	this.carry = 0;
	this.sum = 0;
}

FullAdder.prototype.__defineGetter__("sum", function() {
	let sum0 = new XorGate();
	if(this.in0.value === 1) {
		sum0.in0.on();
	} else {
		sum0.in0.off();
	}
	if(this.in1.value === 1) {
		sum0.in1.on();
	} else {
		sum0.in1.off();
	}
	let sum1 = new XorGate();
	if(sum0.value === 1) {
		sum1.in0.on();
	} else {
		sum1.in0.off();
	}
	if(this.cin.value === 1) {
		sum1.in1.on();
	} else {
		sum1.in1.off();
	}
	this.sum = sum1.value;
	return sum1.value;
});

FullAdder.prototype.__defineGetter__("carry", function() {
	let sum0 = new XorGate();
	if(this.in0.value === 1) {
		sum0.in0.on();
	} else {
		sum0.in0.off();
	}
	if(this.in1.value === 1) {
		sum0.in1.on();
	} else {
		sum0.in1.off();
	}

	let carry0 = new AndGate();
	if(this.cin.value === 1) {
		carry0.in0.on();
	} else {
		carry0.in0.off();
	}
	if(sum0.value === 1) {
		carry0.in1.on();
	} else {
		carry0.in1.off();
	}

	let carry1 = new AndGate();
	if(this.in0.value === 1) {
		carry1.in0.on();
	} else {
		carry1.in0.off();
	}
	if(this.in1.value === 1) {
		carry1.in1.on();
	} else {
		carry1.in1.off();
	}

	let carry = new XorGate();
	if(carry0.value === 1) {
		carry.in0.on();
	} else {
		carry.in0.off();
	}
	if(carry1.value === 1) {
		carry.in1.on();
	} else {
		carry.in1.off();
	}

	this.carry = carry.value;
	return carry.value;
});

module.exports = FullAdder;

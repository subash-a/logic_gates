var Switch = require("./switch");

function OrGate() {
	this.val = 0;
	this.in0 = new Switch();
	this.in1 = new Switch();
}

OrGate.prototype.__defineGetter__("value", function() {return this.in0.value || this.in1.value});

module.exports = OrGate;

var FullAdder = require("./full_adder");
var assert = require("assert");

describe("FullAdder", () => {
	let faValue;
	beforeEach(() => {
		faValue = new FullAdder();
	});

	it("should not throw error when created", () => {
		assert.doesNotThrow(() => new FullAdder());
	});

	describe("when input carry is 0", () => {
		it("should output 1 as carry and 0 as sum when both values are 1", () => {
			faValue.in0.on();
			faValue.in1.on();
			assert.equal(faValue.carry, 1);
			assert.equal(faValue.sum, 0);
		});
		it("should output 1 as sum and 0 as carry when either values are 1", () => {
			faValue.in0.on();
			faValue.in1.off();
			assert.equal(faValue.sum, 1);
			assert.equal(faValue.carry, 0);

			faValue.in0.off();
			faValue.in1.on();
			assert.equal(faValue.sum, 1);
			assert.equal(faValue.carry, 0);
		});
		it("should output 0 as sum and 0 as carry when both values are 0", () => {
			faValue.in0.off();
			faValue.in1.off();
			assert.equal(faValue.sum, 0);
			assert.equal(faValue.carry, 0);
		});

	});

	describe("when input carry is 1", () => {
		it("should output 1 as carry and 0 as sum when both values are 1", () => {
			faValue.in0.on();
			faValue.in1.on();
			faValue.cin.on();
			assert.equal(faValue.sum, 1);
			assert.equal(faValue.carry, 1);
		});
		it("should output 1 as sum and 0 as carry when either values are 1", () => {
			faValue.in0.on();
			faValue.in1.off();
			faValue.cin.on();
			assert.equal(faValue.sum, 0);
			assert.equal(faValue.carry, 1);

			faValue.in0.off();
			faValue.in1.on();
			faValue.cin.on();
			assert.equal(faValue.sum, 0);
			assert.equal(faValue.carry, 1);
		});
		it("should output 0 as sum and 0 as carry when both values are 0", () => {
			faValue.in0.off();
			faValue.in1.off();
			faValue.cin.on();
			assert.equal(faValue.sum, 1);
			assert.equal(faValue.carry, 0);
		});
	});
});

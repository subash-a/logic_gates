var HalfAdder = require("./half_adder");
var assert = require("assert");

describe("HalfAdder", () => {
	let haValue;
	beforeEach(() => {
		haValue = new HalfAdder();
	});

	it("should not throw error when created", () => {
		assert.doesNotThrow(() => new HalfAdder());
	});
	it("should output 1 as carry and 0 as sum when both values are 1", () => {
		haValue.in0.on();
		haValue.in1.on();
		assert.equal(haValue.carry, 1);
		assert.equal(haValue.sum, 0);
	});
	it("should output 1 as sum and 0 as carry when either values are 1", () => {
		haValue.in0.on();
		haValue.in1.off();
		assert.equal(haValue.sum, 1);
		assert.equal(haValue.carry, 0);

		haValue.in0.off();
		haValue.in1.on();
		assert.equal(haValue.sum, 1);
		assert.equal(haValue.carry, 0);
	});
	it("should output 0 as sum and 0 as carry when both values are 0", () => {
		haValue.in0.off();
		haValue.in1.off();
		assert.equal(haValue.sum, 0);
		assert.equal(haValue.carry, 0);
	});
});

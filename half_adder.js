var Switch = require("./switch");
var AndGate = require("./andgate");
var XorGate = require("./xorgate");

function HalfAdder() {
	this.carry = 0;
	this.sum = 0;
	this.in0 = new Switch();
	this.in1 = new Switch();
}

HalfAdder.prototype.__defineGetter__("carry", function() {
	let sum = new AndGate();
	if(this.in0.value === 1) {
		sum.in0.on();
	} else {
		sum.in0.off();
	}
	if(this.in1.value === 1) {
		sum.in1.on();
	} else {
		sum.in1.off();
	}
	this.sum = sum.value;
	return sum.value;
});

HalfAdder.prototype.__defineGetter__("sum", function() {
	let carry = new XorGate();
	if(this.in0.value === 1) {
		carry.in0.on();
	} else {
		carry.in0.off();
	}
	if(this.in1.value === 1) {
		carry.in1.on();
	} else {
		carry.in1.off();
	}
	this.carry = carry.value;
	return carry.value;
});

module.exports = HalfAdder;

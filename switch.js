function Switch() {
	this.val = 0;
}

Switch.prototype.on = function() {
	this.val = 1;
}

Switch.prototype.off = function() {
	this.val = 0;
}

Switch.prototype.__defineGetter__("value", function(){return this.val});

module.exports = Switch;

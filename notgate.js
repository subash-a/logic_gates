var Switch = require("./switch");

function NotGate() {
	this.val = 0;
	this.in = new Switch();
}

NotGate.prototype.__defineGetter__("value", function() { return this.in.value ? 0 : 1});

module.exports = NotGate;

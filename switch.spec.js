var Switch = require("./switch");
var assert = require("assert");

describe("Switch", () => {
	it("should allow creation of new instance", () => {
		assert.doesNotThrow(() => new Switch());
	});

	it("should allow turning on", () => {
		let s = new Switch();
		s.off();
		assert.equal(s.value, 0);
		s.on();
		assert.equal(s.value, 1);
	});

	it("should allow turning off", () => {
		let s = new Switch();
		s.on();
		assert.equal(s.value, 1);
		s.off();
		assert.equal(s.value, 0);
	});
});
